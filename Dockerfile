# Use Alpine Linux as the base image
FROM alpine:latest

# Create a directory for your scripts inside the container
WORKDIR /app

# Copy all shell scripts from the host machine to the container
COPY . .

# Make all shell scripts executable
RUN chmod +x ./scripts/setup.sh
RUN chmod +x ./scripts/run-pulumi.sh

# Run each script in the container
CMD sh  '/app/scripts/setup.sh'

