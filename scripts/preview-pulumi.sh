#!/bin/sh

# exit if a command returns a non-zero exit code and also print the commands and their args as they are executed
set -e -x

# Add the pulumi CLI to the PATH
export PATH=$PATH:$HOME/.pulumi/bin

pulumi stack select pbates85/pulumi-staging/dev

pulumi preview -j # this line will be the pulumi preview command in pulumi-preview.sh