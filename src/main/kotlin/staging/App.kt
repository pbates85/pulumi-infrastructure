package staging

import com.pulumi.Context
import com.pulumi.Pulumi
import com.pulumi.aws.apigateway.Deployment
import com.pulumi.aws.apigateway.DeploymentArgs
import com.pulumi.aws.apigateway.RestApi
import com.pulumi.aws.apigateway.RestApiArgs
import com.pulumi.aws.apigateway.inputs.RestApiEndpointConfigurationArgs
import com.pulumi.aws.ec2.*
import com.pulumi.aws.ec2.inputs.*
import com.pulumi.aws.iam.IamFunctions
import com.pulumi.aws.iam.Role
import com.pulumi.aws.iam.RoleArgs
import com.pulumi.aws.iam.inputs.GetPolicyDocumentArgs
import com.pulumi.aws.iam.inputs.GetPolicyDocumentStatementArgs
import com.pulumi.aws.iam.inputs.GetPolicyDocumentStatementPrincipalArgs
import com.pulumi.codegen.internal.Serialization.*
import com.pulumi.resources.CustomResourceOptions
import java.util.Map
import com.pulumi.aws.lambda.Function;
import com.pulumi.aws.lambda.FunctionArgs;
import com.pulumi.aws.lambda.inputs.FunctionEnvironmentArgs

fun main() {
    Pulumi.run(::stack)
}

fun stack(ctx: Context) {

    // VPC
    val stagingVPC = Vpc("Staging_Pulumi", VpcArgs.builder()
        .cidrBlock("10.105.0.0/16")
        .instanceTenancy("default")
        .tags(mapOf("Name" to "Staging_Pulumi"))
        .build())

    // Subnets
    val stagingPublicSubnets = listOf("10.105.48.0/24","10.105.49.0/24", "10.105.50.0/24").mapIndexed { index, cidrBlock ->
        Subnet("Public_Subnet_${index + 1}", SubnetArgs.builder()
            .vpcId(stagingVPC.id())
            .cidrBlock(cidrBlock)
            .mapPublicIpOnLaunch(true)
            .availabilityZone("us-east-1${('a'.toInt() + index).toChar()}")
            .build()
        )
    }
    val stagingPrivateSubnets = listOf("10.105.0.0/20","10.105.16.0/20", "10.105.32.0/20").mapIndexed { index, cidrBlock ->
        Subnet("Private_Subnet_${index + 1}", SubnetArgs.builder()
            .vpcId(stagingVPC.id())
            .cidrBlock(cidrBlock)
            .mapPublicIpOnLaunch(false)
            .availabilityZone("us-east-1${('a'.toInt() + index).toChar()}")
            .build()
        )
    }

    // IGW
    val public_igw = InternetGateway("Staging IGW", InternetGatewayArgs.builder()
        .vpcId(stagingVPC.id())
        .tags(mapOf("Name" to "Staging"))
        .build())

    // Public route IGW
    val publicStagingRouteTable = RouteTable("StagingPublicRT", RouteTableArgs.builder()
        .vpcId(stagingVPC.id())
        .routes(
            RouteTableRouteArgs.builder()
            .cidrBlock("0.0.0.0/0")
            .gatewayId(public_igw.id())
            .build()
        )
        .tags(mapOf("Name" to "pubsn_igw"))
        .build())

    val routeTableAssociations = stagingPublicSubnets.mapIndexed { index, subnet ->
        RouteTableAssociation("routeTableAssociation_$index", RouteTableAssociationArgs.builder()
            .subnetId(subnet.id())
            .routeTableId(publicStagingRouteTable.id())
            .build()
        )
    }

    // NAT

    val natEIP = Eip(
        "StagingNAT", EipArgs.builder()
            .build()
    )

    val stagingNAT =
        NatGateway(
            "StagingNAT", NatGatewayArgs.builder()
                .allocationId(natEIP.id())
                .subnetId(stagingPrivateSubnets[0].id())
                .tags(Map.of("Name", "gw NAT"))
                .build(), CustomResourceOptions.builder()
                .dependsOn(public_igw)
                .build()
        )

    val publicPrivateRouteTable = RouteTable("StagingPrivateRT", RouteTableArgs.builder()
        .vpcId(stagingVPC.id())
        .routes(
            RouteTableRouteArgs.builder()
                .cidrBlock("0.0.0.0/0")
                .gatewayId(stagingNAT.id())
                .build()
        )
        .tags(mapOf("Name" to "prisn_nat"))
        .build())

    val natTableAssociations = stagingPrivateSubnets.mapIndexed { index, subnet ->
        RouteTableAssociation("NATTableAssociation_$index", RouteTableAssociationArgs.builder()
            .subnetId(subnet.id())
            .routeTableId(publicPrivateRouteTable.id())
            .build()
        )
    }

    // Security Groups
    val allowSSH = SecurityGroup("allowSSH", SecurityGroupArgs.builder()
        .description("Allow SSH inbound traffic")
        .vpcId(stagingVPC.id())
        .ingress(
            SecurityGroupIngressArgs.builder()
                .description("SSH")
                .fromPort(22)
                .toPort(22)
                .protocol("tcp")
                .cidrBlocks("0.0.0.0/0")
                .ipv6CidrBlocks("::/0")
                .build()
        )
        .egress(
            SecurityGroupEgressArgs.builder()
                .fromPort(0)
                .toPort(0)
                .protocol("-1")
                .cidrBlocks("0.0.0.0/0")
                .ipv6CidrBlocks("::/0")
                .build()
        )
        .tags(mapOf("Name" to "allow_ssh"))
        .build()
    )


    // EC2

    // Public Bastion

    val bastion = Instance("Bastion", InstanceArgs.builder()
        .ami("ami-06aa3f7caf3a30282")
        .instanceType("t3.micro")
        .keyName("pulumi")
        .associatePublicIpAddress(true)
        .subnetId(stagingPublicSubnets[0].id())
//        .vpcSecurityGroupIds("sg-061a162830b425d36")
        .tags(mapOf("Name" to "Public Bastion"))
        .build()
    )
    // Private Instance
    val private_instance = Instance("Private Instance", InstanceArgs.builder()
        .ami("ami-06aa3f7caf3a30282")
        .instanceType("t3.micro")
        .keyName("pulumi")
        .associatePublicIpAddress(false)
        .subnetId(stagingPrivateSubnets[0].id())
//        .vpcSecurityGroupIds("sg-061a162830b425d36")
        .tags(mapOf("Name" to "Private Bastion"))
        .build()
    )

    // API Gateway
    val stagingRestApi = RestApi(
        "StagingRestApi",
        RestApiArgs.builder()
            .body(
                serializeJson(
                    jsonObject(
                        jsonProperty("openapi", "3.0.1"),
                        jsonProperty("info", jsonObject(
                            jsonProperty("title", "staging"),
                            jsonProperty("version", "1.0")
                        )),
                        jsonProperty("paths", jsonObject(
                            jsonProperty("/path", jsonObject(
                                jsonProperty("get", jsonObject(
                                    jsonProperty("x-amazon-apigateway-integration", jsonObject(
                                        jsonProperty("httpMethod", "GET"),
                                        jsonProperty("payloadFormatVersion", "1.0"),
                                        jsonProperty("type", "HTTP_PROXY"),
                                        jsonProperty("uri", "https://ip-ranges.amazonaws.com/ip-ranges.json")
                                    ))
                                ))
                            ))
                        ))
                    )
                ))
                .endpointConfiguration(
                    RestApiEndpointConfigurationArgs.builder().types("REGIONAL").build())
                .build()
            )

    val stagingDeployment = Deployment(
        "stagingDeployment", DeploymentArgs.builder()
            .restApi(stagingRestApi.id())
            .build()
    )

    // S3


    // Lambda
    val assumeRole = IamFunctions.getPolicyDocument(
        GetPolicyDocumentArgs.builder()
        .statements(
            GetPolicyDocumentStatementArgs.builder()
                .effect("Allow")
                .principals(
                    GetPolicyDocumentStatementPrincipalArgs.builder()
                        .type("Service")
                        .identifiers("lambda.amazonaws.com")
                        .build()
                )
                .actions("sts:AssumeRole")
                .build()
        )
        .build()
    )

    val iamForLambda = Role("iamForLambda", RoleArgs.builder()
        .assumeRolePolicy(assumeRole.applyValue { it.json() })
        .build()
    )


    val stagingLambda = Function("stagingLambda", FunctionArgs.builder()
        .role(iamForLambda.arn())
        .packageType("Image")
        .runtime("java11")
        .architectures("x86_64")
        .imageUri("161660187510.dkr.ecr.us-east-1.amazonaws.com/simple-java:latest")
        .build()
    )


}


// AMI LOOKUP
//    val ubuntu = Ec2Functions.getAmi(
//        GetAmiArgs.builder()
//        .mostRecent(true)
//        .filters(
//            GetAmiFilterArgs.builder()
//                .name("name")
//                .values("ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*")
//                .build(),
//            GetAmiFilterArgs.builder()
//                .name("virtualization-type")
//                .values("hvm")
//                .build()
//        )
//        .owners("161660187510")
//        .build()
//    )